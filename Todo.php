<?php
class Todo{

    public function conectarBD(){
        return new mysqli('127.0.0.1', 'root', '', 'db');
    }

    public function criarTarefa($tarefa){
        $bd = $this->conectarBD();
        $data = date("Y-m-d");
        $sql = "INSERT INTO todo (tarefa, completa, data) VALUES ('$tarefa', '0', '$data')";
        if ($bd->query($sql)) {
        } else {
            echo "Error: " . $sql . "<br>" . $bd->error;
        }
    }

    public function removerTarefa($tarefa){
        $bd = $this->conectarBD();
        $sql = "DELETE FROM todo WHERE todo.tarefa = '$tarefa'";
        if ($bd->query($sql)) {
            //echo "tarefa removida";
        } else {
            echo "Error: " . $sql . "<br>" . $bd->error;
        }
    }

    public function atualizarTarefa($tarefa, $completa){
        $bd = $this->conectarBD();
        $sql = "UPDATE todo set completa = $completa WHERE todo.tarefa = '$tarefa'";
        if ($bd->query($sql)) {
            //echo "tarefa atualizada";
        } else {
            echo "Error: " . $sql . "<br>" . $bd->error;
        }
    }

    public function getTarefas(){
        $bd = $this->conectarBD();
        $sql = "SELECT * FROM todo";
        return $bd->query($sql);
    }

    public function getTarefasEspecificas($filtro){
        $bd = $this->conectarBD();
        $sql = "SELECT * FROM todo $filtro";
        return $bd->query($sql);
    }

}
