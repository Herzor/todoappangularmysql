var app = angular.module('TodoManager', []);
app.controller('TodoController', function($scope, $http) {
//Inicia com as tarefas já cadastradas
    getTarefas("");

    function getTarefas(filtro){
        $http({
            url: "gerenciador.php",
            method: "SEND",
            data: {
                filtro: filtro,
                funcao: "getTarefas"
            }
        }).then(function(response) {
            atualizaTela(response)
        }, function(error){
            alert(JSON.stringify(error));
        });
    };

    function atualizaTela(response){
        $scope.saved = response.data;
        $scope.todoItems = $scope.saved;
        var todoBanco = $scope.saved;
        $scope.todoItems = [];
        angular.forEach(todoBanco, function (todoItem) {
            $scope.todoItems.push({
                tarefa: todoItem.tarefa,
                completa: (todoItem.completa == 0) ? false : true
            });
        });
    };

    $scope.getTodas = function() {
        getTarefas("");
    };

    $scope.getCompletas = function() {
        angular.element(document.querySelector("#filtroTodas")).removeClass("button-large");
        getTarefas("WHERE completa = 1");
    };

    $scope.getIncompletas = function() {
        angular.element(document.querySelector("#filtroTodas")).removeClass("button-large");
        getTarefas("WHERE completa = 0");
    };

    $scope.addTodo = function() {
        var nomeTarefa = $scope.newTodo;
        if(nomeTarefa != null && nomeTarefa != ""){
            $http({
                url: "gerenciador.php",
                method: "SEND",
                data: {
                    tarefa: nomeTarefa,
                    funcao: "criaTarefa"
                }
            }).then(function(response) {
                //alert(JSON.stringify(response));
            }, function(error){
                alert(JSON.stringify(error));
            });
            $scope.todoItems.push({
                tarefa: $scope.newTodo,
                completa: false
            });
            $scope.newTodo = '';
        }
    };

    $scope.save = function () {
        var doneTodo = $scope.todoItems;
        angular.forEach(doneTodo, function(todoItem) {
            var tarefa = todoItem.tarefa;
            var completa = (todoItem.completa == false) ? 0 : 1;
            $http({
                url: "gerenciador.php",
                method: "SEND",
                data: {
                    tarefa: tarefa,
                    completa: completa,
                    funcao: "atualizarTarefa"
                }
            }).then(function(response) {
                //alert(JSON.stringify(response));
            }, function(error){
                alert(JSON.stringify(error));
            });
        });
    };

    $scope.deleteSelected = function () {
        var doneTodo = $scope.todoItems;
        $scope.todoItems = [];
        angular.forEach(doneTodo, function(todoItem) {
            if (todoItem.completa == true) {
                var tarefa = todoItem.tarefa;
                $http({
                    url: "gerenciador.php",
                    method: "SEND",
                    data: {
                        tarefa: tarefa,
                        funcao: "removerTarefa"
                    }
                }).then(function(response) {
                    //alert(JSON.stringify(response));
                }, function(error){
                    alert(JSON.stringify(error));
                });

            }else{
                $scope.todoItems.push(todoItem);
            }
        });
    };

    $scope.remaining = function() {
        var count = 0;
        angular.forEach($scope.todoItems, function(todo){
            count+= (todo.completa == 0) ? 0 : 1;
        });
        return count;
    };
});