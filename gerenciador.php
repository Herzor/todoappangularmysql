<?php
include("Todo.php");

$todo = new Todo();
$request = file_get_contents("php://input");
$dados = json_decode($request);

$operacao = $dados->funcao;

switch ($operacao){
    case "removerTarefa":
        $todo->removerTarefa($dados->tarefa);
        break;
    case "atualizarTarefa":
        $todo->atualizarTarefa($dados->tarefa, $dados->completa);
        break;
    case "criaTarefa":
        $todo->criarTarefa($dados->tarefa);
        break;
    case "getTarefas":
        if($dados->filtro == null) {
            $resposta = $todo->getTarefas();
        }else{
            $resposta = $todo->getTarefasEspecificas($dados->filtro);
        }
        $arrayResposta = array();
        if(mysqli_num_rows($resposta) != 0) {
            while ($linha = mysqli_fetch_assoc($resposta)) {
                $arrayResposta[] = $linha;
            }
        }
        echo $json_info = json_encode($arrayResposta);
        break;
}